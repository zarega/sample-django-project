# Sample Django Project

This is a starting point for a Django project.

## Setup Project Folder

The project folder is named based on the project. It will be checked into
source control. I use [Poetry](https://python-poetry.org/) for dependency management.

```
mkdir sample-django-project
cd sample-django-project
poetry init
git init
curl -o .gitignore https://www.toptal.com/developers/gitignore/api/django
vim README.md
```

For the rest of this document, all poetry commands are done in the
sample-django-project folder, while tree is run from the sample-django-project
parent folder.

## Create the Django Project

Create a Django project called "project". I like this idea, so I can think
about project settings (project/settings.py) and project urls
(project/urls.py), etc.

```
poetry add django
poetry run django-admin startproject project .
```

The project folder now looks like the following:

```
tree -L 2 sample-django-project
sample-django-project
├── manage.py
├── poetry.lock
├── project
│   ├── asgi.py
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── pyproject.toml
└── README.md

1 directory, 9 files
```

## Install Django Extensions

I think I used [Django Extensions](https://django-extensions.readthedocs.io/en/latest/) initially for RunServerPlus and its SSL options, but there is so much more. Mainly, I use shell_plus and jobs scheduling.

```
poetry add django-extensions
```

Add it to the INSTALLED_APPS in sample-django-project/project/settings.py:

```
INSTALLED_APPS = [
  ...
  'django_extensions',
]
```

## Create an Application

Name each app something meaningful.

```
poetry run ./manage.py startapp core
```

Add it to the INSTALLED_APPS in sample-django-project/project/settings.py:

```
INSTALLED_APPS = [
  ...
  'core.apps.CoreConfig',
]
```

## Create Jobs using Django Extensions

Since we installed Django Extensions, we can use Jobs Scheduling. Jobs are
created in a specific app.

```
poetry run ./manage.py create_jobs core
```

The core app now includes a jobs directory:

```
tree -L 1 sample-django-project/core/jobs
sample-django-project/core/jobs
├── daily
├── hourly
├── __init__.py
├── monthly
├── sample.py
├── weekly
└── yearly

5 directories, 2 files
```

